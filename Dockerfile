FROM php:7.4-apache
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_PID_FILE /var/run/apache2/apache2.pid
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
COPY . /var/www/html/
RUN docker-php-ext-install mysqli && a2enmod rewrite
CMD ["apache2", "-D", "FOREGROUND"]